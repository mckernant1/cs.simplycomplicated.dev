#!/usr/bin/env bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install zsh curl git wget
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
brew cask
brew install rustup-init tree less grip python sl fswatch node pipenv vim bat autojump jq gradle kotlin
brew cask install font-meslo-for-powerline intellij-idea visual-studio-code google-chrome firefox iterm2 docker sublime-text flux postman
exec zsh
