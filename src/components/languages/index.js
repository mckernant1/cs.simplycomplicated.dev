import Bash from './Bash.vue'
import Python from './Python.vue'
import Helpers from './Helpers.vue'

export default [Bash, Python, Helpers]
