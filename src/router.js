import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import lingos from './components/languages/index'

Vue.use(Router)

let routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  }
]

lingos.forEach(value => {
  routes.push({
    path: `/${value.name.toLowerCase()}`,
    name: value.name,
    component: value
  })
})

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: routes
})
